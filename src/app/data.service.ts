import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Observable, from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  getUrl = "https://ws.quantatel.com/ikhair/causes?country=AE"
  constructor(private http: HTTP , private HttpClient: HttpClient) { }


  GetRequest()  : Observable<any> {

    return from(this.http.get(this.getUrl,{},{}))

  }

  getRrquestTest(){

    return this.HttpClient.get(this.getUrl)

  }

}
