import { Component,ElementRef ,OnInit,ViewChild} from '@angular/core';
import  {DataService} from '../data.service'
import { Chart } from 'chart.js';
import { SMS } from '@ionic-native/sms/ngx';
import { LoadingController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild("doughnutCanvas",{static:false}) doughnutCanvas: ElementRef;
  
  private doughnutChart: Chart;
  public loader: HTMLIonLoadingElement;
  results;
  receiverNumber;
  cause;
  CollectedAmount;
  TargetAmount;
  Info;
  Sms;
  calc;
  interval: any;

  constructor( private DataService : DataService,private sms: SMS,public loadingController: LoadingController,private socialSharing: SocialSharing) {

    console.log(this.calc)

  }

   async showLoader() {
    if (!this.loader) {
        this.loader = await this.loadingController.create({
          spinner: null,
          duration: 5000,
          message: 'Please wait...',
          translucent: true,
          cssClass: 'custom-class custom-loading'
        });
    }
    await this.loader.present();
}

async hideLoader() {
  if (this.loader) {
      await this.loader.dismiss();
      this.loader = null;
  }
}


  GetData(){


    this.DataService.GetRequest().subscribe((data) => {
      this.results = JSON.parse(data.data)
      this.cause = this.results.ikhair.country[0]['-causes'].cause[0]
      
     this.CollectedAmount = this.cause['-collectedAmount']
      this.Info= this.cause['-info']
      this.TargetAmount= this.cause['-targetAmount']
      this.receiverNumber = this.cause.organization.paymentMethods.SMS[0]['-shortcode']
      this.calc= this.CollectedAmount / this.TargetAmount * 100
      this.charts()

      this.hideLoader()
      this.interval = setInterval(() => { 
        this.GetData(); 
    }, 30000);
    })
  }

  charts() {

    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: "doughnut",
      options: {
        tooltips: {
             enabled: false
        }
    },
      data: {
        datasets: [
          {
            data: [100],
            backgroundColor: [
              "#0cd1e8",

              
            ],
            hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56"]
          }, {
            data: [this.calc,this.calc - 100],
            backgroundColor: [
              "rgba(255, 99, 132,)",
              "rgba(255, 99, 132,0)",

            ],
            hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56"]
          }
        ]
      }
    });

  }

  SendSms(){
    this.socialSharing.shareViaSMS("",this.receiverNumber)
  }

  shareWithTwitter() {
    this.socialSharing.shareViaTwitter("Donate For Childern", null, null)
  }
  shareWithFacebook(){
    this.socialSharing.shareViaFacebook("Donate For Childern", null,null)
  }
  shareNative(){

    this.socialSharing.share('Donate For Childern',null,null)
  }
  ngOnInit(){
    this.showLoader();
    this.GetData()
  

  }
  ionViewDidLeave(){
   
  }

  ngAfterViewInit() {

  

  }

}
